﻿using Microsoft.AspNetCore.Mvc;

namespace MVFactoryWeatherTest.Front.Controllers
{
    public class AuthenticationController : Controller
    {
        public IActionResult Login()
        {
            return View("Login");
        }
    }
}
