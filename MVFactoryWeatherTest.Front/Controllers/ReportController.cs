﻿using Microsoft.AspNetCore.Mvc;

namespace MVFactoryWeatherTest.Front.Controllers
{
    public class ReportController : Controller
    {
        public IActionResult RenderForm()
        {
            return View("Form");
        }
    }
}
