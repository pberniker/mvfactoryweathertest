﻿function callAjax(endPoint, authorizationType, authorizationValue, isPost, data, successFunction, errorFunction) {
    $('#loader').show();
    $('#alert').hide();

    var url = window['apiUrl'] + '/' + endPoint;
    var authorization = authorizationType + ' ' + authorizationValue;
    var headers = { 'Authorization': authorization };

    var options = {
        url: url,
        type: isPost ? 'post' : 'get',
        dataType: 'json',
        headers: headers,
        cache: false,
        success: function(res) {
            if (typeof successFunction === 'function') { successFunction(res); }
            $('#loader').hide();
        },
        error: function (jaXHR, status, error) {
            var statusCode = jaXHR.status;

            console.log('jaXHR:', jaXHR);
            console.log('statusCode:', statusCode);
            console.log('status:', status);

            if (statusCode == 401) {
                window.location.href = '/login';
                return;
            }

            if (typeof errorFunction === 'function') {
                var res = jaXHR.responseJSON
                    ? jaXHR.responseJSON
                    : jaXHR.responseText
                    ? JSON.parse(jaXHR.responseText)
                    : {};

                errorFunction(res, statusCode);
            }

            $('#alert').show();
            $('#loader').hide();
        }
    };

    if (typeof data === 'object') {
        if (isPost) {
            options['contentType'] = 'json';
            options['data'] = JSON.stringify(data);
        }
        else {
            options['data'] = data;
        }
    }        

    console.clear();
    console.log('Call ajax to ' + url + ' ...');
    console.log('options', options);
    console.log();

    $.ajax(options);
}