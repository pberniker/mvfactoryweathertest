﻿$(function () {
    $('#frm').validate({ debug: true });

    $('#login').click(function () {
        var valid = $('#frm').valid();
        if (!valid) { return; }
        var username = $('#username').val();
        var password = $('#password').val();
        callAjax('authentication', 'Basic', btoa(username + ':' + password), true, {}, successFunction);
    });
});

function successFunction(res) {
    console.log('res:', res);
    localStorage.setItem('token', res.token);
    window.location.href = '/report';
}