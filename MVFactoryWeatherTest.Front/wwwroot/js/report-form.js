﻿$(function () {
    var token = localStorage.getItem('token');

    console.log('token:', token);

    if (!token) { window.location.href = '/login'; }

    $('#frm').validate({ debug: true });

    $('#generate').click(function () {
        $('#report').hide();
        $('#body').html('');
        var valid = $('#frm').valid();
        if (!valid) { return; }
        var data = { from: toIso('from', '00:00:00'), to: toIso('to', '23:59:59') };
        $('#loader').show();
        callAjax('history', 'Bearer', token, false, data, successFunction);
    });

    var options = {
        defaultDate: '+1w',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 3,
        showButtonPanel: true,
        dateFormat: "dd/mm/yy",
        timeFormat: "hh:mm"
    };

    var from, to;

    from = $('#from').datepicker(options).on('change', function () {
        to.datepicker('option', 'minDate', getDateTime(this));
    });

    to = $('#to').datepicker(options).on('change', function () {
        from.datepicker('option', 'maxDate', getDateTime(this));
    });
});

function getDateTime(el) {
    try {
        var res = $.datepicker.parseDate('dd/mm/yy', el.value);
        return res;
    }
    catch (error) {
        return '';
    }
}

function toIso(id, time) {
    try {
        var val = $('#' + id).val();
        var date = val.split('/');
        var res = date[2] + date[1] + date[0] + 'T' + time.replace(/:/g, '');
        return res;
    }
    catch (error) {
        return '';
    }
}

function successFunction(res) {
    console.log('res:', res);

    var length = res.length;

    if (length == 0) {
        $('#alert').show();
        return;
    }

    for (var j = 0; j < length; j++) {
        var item = res[j];
        var date = item.date;
        var temp = item.main.temp;
        $('#body').append('<tr><td>' + date + '</td><td>' + temp + '</td></tr>');
    }

    $('#report').show();
}