﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MVFactoryWeatherTest.Helpers
{
    public static class EncodeHelper
    {
        public static string ToMd5(string val)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(val);
            var hashBytes = md5.ComputeHash(inputBytes);
            var stringBuilder = new StringBuilder();

            foreach (var b in hashBytes)
            {
                var str = b.ToString("X2");
                stringBuilder.Append(str);
            }

            var res = stringBuilder.ToString();

            return res;
        }

        public static string Base64Decode(string val)
        {
            byte[] data = Convert.FromBase64String(val);
            var res = Encoding.UTF8.GetString(data);
            return res;
        }
    }
}
