using MvFactoryWeatherTest.Domain;
using MvFactoryWeatherTest.Service.Concretes;
using Newtonsoft.Json;
using NUnit.Framework;

namespace MVFactoryWeatherTest.Test.Service
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void MapTest()
        {
            // Arrange

            const string data =
                "{ \"coord\":{ \"lon\":-58.45,\"lat\":-34.6},\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":295.41,\"feels_like\":293.52,\"temp_min\":293.15,\"temp_max\":297.04,\"pressure\":1020,\"humidity\":73},\"visibility\":10000,\"wind\":{\"speed\":6.2,\"deg\":110},\"clouds\":{\"all\":75},\"dt\":1583847127,\"sys\":{\"type\":1,\"id\":8224,\"country\":\"AR\",\"sunrise\":1583833760,\"sunset\":1583878726},\"timezone\":-10800,\"id\":3433955,\"name\":\"Buenos Aires F.D.\",\"cod\":200}";

            // Act

            var res = JsonConvert.DeserializeObject<OpenWeatherData>(data);

            // Assert

            Assert.AreEqual(res.Coord.Lon, "-58.45");
        }

        [Test]
        public void GetDataTest()
        {
            // Arrange

            const string url = "http://api.openweathermap.org/data/2.5";
            const string key = "54e7e25b76dd657a188618e9ae7924bb";
            const int cityId = 3433955;

            var openWeatherService = new OpenWeatherService(url, key);

            // Act

            var res = openWeatherService.GetData(cityId);

            // Assert

            Assert.IsNotEmpty(res);
        }
    }
}