﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MvFactoryWeatherTest.Api.Exceptions;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Helpers;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MVFactoryWeatherTest.Api.Controllers
{
    [ApiController]
    public class AuthenticationController : BaseController
    {
        #region Members
        private readonly IBranchRepository _branchRepository;
        #endregion

        #region Construct
        public AuthenticationController(IServiceProvider serviceProvider, IConfiguration configuration) : base(configuration)
        {
            _branchRepository = serviceProvider.GetService<IBranchRepository>();
        }
        #endregion

        #region Public

        [Route("authentication")]
        [HttpPost]
        public ActionResult Login()
        {
            try
            {
                var branch = GetBranchFromAuthorizationHeader();
                var expiredIn = Convert.ToInt16(Configuration["Jwt:ExpiredInMinutes"]);
                var expiredAt = DateTime.Now.AddMinutes(expiredIn);
                var token = GenerateToken(branch, expiredAt);

                var obj = new
                    {
                        token,
                        expiredIn = $"+ {expiredIn} minute(s)",
                        expiredAt = expiredAt.ToString("yyyy-MM-ddTHH\\:mm\\:ss")
                    };

                var res = GetResponse(obj);

                return res;
            }
            catch (BaseValidationException ex)
            {
                return GetResponse(ex);
            }
            catch (Exception ex)
            {
                return GetResponse(ex);
            }
        }

        #endregion

        #region Private

        private Branch GetBranchFromAuthorizationHeader()
        {
            const string type = "Basic";

            var authorization = GetAuthorizationHeaderValue();

            if (string.IsNullOrEmpty(authorization)) { throw new RequiredException("Authorization"); }

            var indexOf = authorization.IndexOf(type);

            if (indexOf == -1) { throw new InvalidException("Authorization"); }

            var arr = authorization.Split("Basic");

            if (arr.Length != 2) { throw new InvalidException("Authorization"); }

            var decode = EncodeHelper.Base64Decode(arr[1]);

            if (string.IsNullOrEmpty(decode)) { throw new InvalidException("Authorization"); }

            arr = decode.Split(':');

            if (arr.Length != 2) { throw new InvalidException("Authorization"); }

            var username = arr[0];
            var password = arr[1];

            var branch = _branchRepository.Get(username, password);

            if (branch == null) { throw new UnauthorizedException(); }

            return branch;
        }

        private string GetAuthorizationHeaderValue()
        {
            Request.Headers.TryGetValue("Authorization", out var authorization);
            var res = authorization.Count == 1 ? authorization.ToString() : "";
            return res;
        }

        private string GenerateToken(Branch branch, DateTime expiredAt)
        {
            var key = Configuration["Jwt:Key"];

            var tokenHandler = new JwtSecurityTokenHandler();
            var encodingKey = Encoding.ASCII.GetBytes(key);

            var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new []
                        {
                            new Claim(ClaimTypes.Name, branch.Id.ToString())
                        }
                    ),
                    Expires = expiredAt,
                    NotBefore = DateTime.Now, 
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(encodingKey), SecurityAlgorithms.HmacSha256Signature)
                };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var res = tokenHandler.WriteToken(token);

            return res;
        }

        #endregion
    }
}
