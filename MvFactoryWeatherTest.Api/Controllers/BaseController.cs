﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MvFactoryWeatherTest.Api.Exceptions;

namespace MVFactoryWeatherTest.Api.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        #region Properties
        protected IConfiguration Configuration { get; }
        #endregion

        #region Construct
        protected BaseController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region Protected

        protected ActionResult GetResponse(object obj)
        {
            var res = GetResponse(obj, HttpStatusCode.OK);
            return res;
        }

        protected ActionResult GetResponse(Exception ex)
        {
            var obj = new { message = ex.Message };
            var statusCode = ex is BaseValidationException ? ((BaseValidationException) ex).StatusCode : HttpStatusCode.InternalServerError;
            var res = GetResponse(obj, statusCode);
            return res;
        }

        #endregion

        #region Private

        private ActionResult GetResponse(object obj, HttpStatusCode statusCode)
        {
            var i = (int) statusCode;
            var res = StatusCode(i, obj);
            return res;
        }

        #endregion
    }
}
