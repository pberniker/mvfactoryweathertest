﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MvFactoryWeatherTest.Domain;
using MvFactoryWeatherTest.Service.Interfaces;

namespace MVFactoryWeatherTest.Api.Controllers
{
    [Route("history")]
    [ApiController]
    public class HistoryController : BaseController
    {
        #region Members
        private readonly IWeatherService _weatherService;
        #endregion

        #region Construct
        public HistoryController(IServiceProvider serviceProvider, IConfiguration configuration) : base(configuration)
        {
            _weatherService = serviceProvider.GetService<IWeatherService>();
        }
        #endregion

        #region Public

        [HttpPost]
        public void Persist()
        {
            _weatherService.Persist();
        }

        [Authorize]
        [HttpGet]
        public IEnumerable<OpenWeatherData> Get(string from, string to)
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var claims = identity.Claims.ToList();
            var name = claims.FirstOrDefault(x => x.Type == ClaimTypes.Name);
            var branchId = Convert.ToInt32(name.Value);

            var fromDate = StringToDate(from);
            var toDate = StringToDate(to);
            
            var history = _weatherService.GetHistoryByBranch(branchId, fromDate, toDate);
            var res = history.Select(h => h.Data).ToList();

            return res;
        }

        #endregion

        #region Private

        private static DateTime StringToDate(string str)
        {
            var sucessfully = DateTime.TryParseExact(str, @"yyyyMMdd\THHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var dte);
            var res = sucessfully ? dte : DateTime.Now;
            return res;
        }

        #endregion
    }
}
