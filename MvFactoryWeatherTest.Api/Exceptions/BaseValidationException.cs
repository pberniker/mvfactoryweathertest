﻿using System;
using System.Net;

namespace MvFactoryWeatherTest.Api.Exceptions
{
    public abstract class BaseValidationException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        protected BaseValidationException(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest) : base(message)
        {
            StatusCode = statusCode;
        }
    }
}
