﻿namespace MvFactoryWeatherTest.Api.Exceptions
{
    public class InvalidException : BaseValidationException
    {
        public InvalidException(string variable) : base($"{variable} is invalid")
        {
        }
    }
}
