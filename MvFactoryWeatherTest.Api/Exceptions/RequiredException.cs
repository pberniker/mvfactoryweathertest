﻿namespace MvFactoryWeatherTest.Api.Exceptions
{
    public class RequiredException : BaseValidationException
    {
        public RequiredException(string variable) : base($"{variable} is required")
        {
        }
    }
}
