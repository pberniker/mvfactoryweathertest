﻿using System.Net;

namespace MvFactoryWeatherTest.Api.Exceptions
{
    public class UnauthorizedException : BaseValidationException
    {
        public UnauthorizedException() : base("Unauthorized", HttpStatusCode.Unauthorized)
        {
        }
    }
}
