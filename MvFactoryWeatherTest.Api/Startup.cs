﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MvFactoryWeatherTest.Service.Concretes;
using MvFactoryWeatherTest.Service.Interfaces;
using MVFactoryWeatherTest.Repository;
using MVFactoryWeatherTest.Repository.Concretes;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MVFactoryWeatherTest.Api
{
    public class Startup
    {
        #region Properties
        public IConfiguration Configuration { get; }
        #endregion

        #region Construct

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion

        #region Public

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            ConfigureIocService(services);
            services.AddCors();
            services.AddMvc();
            ConfigureJwtToken(services);
            ConfigureSwaggerService(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) { app.UseDeveloperExceptionPage(); }
            ConfigureCors(app);
            app.UseAuthentication();
            ConfigureSwagger(app);
            app.UseMvc();
        }

        #endregion

        #region Private

        private void ConfigureJwtToken(IServiceCollection services)
        {
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                }
            ).AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["Jwt:Key"])),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    }
            );
        }

        private void ConfigureIocService(IServiceCollection services)
        {
            // DB Context.
            var connectionString = Configuration.GetConnectionString("Default");
            services.AddScoped<MvFactoryWeatherTestDbContext, MvFactoryWeatherTestDbContext>(x => new MvFactoryWeatherTestDbContext(connectionString));

            // Services.
            services.AddScoped<IWeatherService, WeatherService>();

            var url = Configuration.GetValue<string>("OpenWeatherApi:Url");
            var key = Configuration.GetValue<string>("OpenWeatherApi:Key");
            services.AddScoped<IOpenWeatherService, OpenWeatherService>(x => new OpenWeatherService(url, key));

            // Repositories.
            services.AddScoped<IBranchRepository, BranchRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IHistoryRepository, HistoryRepository>();
        }

        private static void ConfigureCors(IApplicationBuilder app)
        {
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
            );
        }

        private static void ConfigureSwaggerService(IServiceCollection services)
        {
            var info = new OpenApiInfo
                {
                    Version = "v1",
                    Title = "API",
                    Description = "ASP.NET Core Web API"
                };

            services.AddSwaggerGen(x => { x.SwaggerDoc("v1", info); });
        }

        private static void ConfigureSwagger(IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(x =>
                {
                    x.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                    x.RoutePrefix = string.Empty;
                }
            );
        }

        #endregion
    }
}
