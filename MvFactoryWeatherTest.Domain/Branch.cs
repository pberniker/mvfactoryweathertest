﻿namespace MVFactoryWeatherTest.Domain
{
    public class Branch
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }

        public City City { get; set; }
    }
}
