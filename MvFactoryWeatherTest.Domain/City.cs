﻿using System.Collections.Generic;

namespace MVFactoryWeatherTest.Domain
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int OpenWeatherId { get; set; }

        public ICollection<Branch> Branches { get; set; }
        public ICollection<History> Histories { get; set; }
    }
}
