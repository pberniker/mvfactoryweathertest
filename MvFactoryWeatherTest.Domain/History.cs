﻿using System;
using MvFactoryWeatherTest.Domain;
using Newtonsoft.Json;

namespace MVFactoryWeatherTest.Domain
{
    public class History
    {
        public int Id { get; set; }
        public City City { get; set; }
        public string JsonData { get; set; }

        public DateTime Date { get; set; }

        public OpenWeatherData Data
        {
            get
            {
                var res = JsonConvert.DeserializeObject<OpenWeatherData>(JsonData);
                res.Date = Date;
                return res;
            }
        }
    }
}
