﻿using System;
using System.Collections.Generic;

namespace MvFactoryWeatherTest.Domain
{
    public class OpenWeatherData
    {
        public DateTime Date { get; set; }
        public CoordDto Coord { get; set; }
        public IEnumerable<WeatherDto> Weathers { get; set; }
        public string Base { get; set; }
        public MainDto Main { get; set; }
        public int Visibility { get; set; }
        public WinDto Win { get; set; }
        public CloudsDto Clouds { get; set; }
        public int Dt { get; set; }
        public SysDto Sys { get; set; }
        public int TimeZone { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cod { get; set; }
    }

    public class CoordDto
    {
        public string Lon { get; set; }
        public string Lat { get; set; }
    }

    public class WeatherDto
    {
        public int Id { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }

    public class MainDto
    {
        public string Temp { get; set; }
        public int FeelsLike { get; set; }
        public int TempMin { get; set; }
        public int TempMax { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
    }

    public class WinDto
    {
        public int Speed { get; set; }
        public int Deg { get; set; }
    }

    public class CloudsDto
    {
        public int All { get; set; }
    }

    public class SysDto
    {
        public int Type { get; set; }
        public int Id { get; set; }
        public string Coutry { get; set; }
        public int Sunrise { get; set; }
        public int Sunset { get; set; }
    }
}
