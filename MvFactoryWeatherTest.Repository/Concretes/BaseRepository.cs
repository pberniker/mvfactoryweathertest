﻿using System;

namespace MVFactoryWeatherTest.Repository.Concretes
{
    public abstract class BaseRepository
    {
        #region Members
        protected MvFactoryWeatherTestDbContext MvFactoryWeatherTestDbContext { get; }
        #endregion

        #region Construct
        protected BaseRepository(MvFactoryWeatherTestDbContext mvFactoryWeatherTestDbContext)
        {
            MvFactoryWeatherTestDbContext = mvFactoryWeatherTestDbContext;
        }
        #endregion

        #region Protected

        protected void Save()
        {
            try
            {
                MvFactoryWeatherTestDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                // Todo: Manage exception.
                throw ex;
            }
        }

        #endregion
    }
}
