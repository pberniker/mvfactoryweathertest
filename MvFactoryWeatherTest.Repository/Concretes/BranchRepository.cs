﻿using System.Linq;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Helpers;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MVFactoryWeatherTest.Repository.Concretes
{
    public class BranchRepository : BaseRepository, IBranchRepository
    {
        public BranchRepository(MvFactoryWeatherTestDbContext mvFactoryWeatherTestDbContext) : base(mvFactoryWeatherTestDbContext)
        {
        }
        
        public Branch Get(string username, string password)
        {
            var res = MvFactoryWeatherTestDbContext.Branchs.FirstOrDefault(x => x.Username == username && x.Password == EncodeHelper.ToMd5(password));
            return res;
        }
    }
}