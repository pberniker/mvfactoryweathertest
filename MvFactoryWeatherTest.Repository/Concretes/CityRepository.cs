﻿using System.Collections.Generic;
using System.Linq;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MVFactoryWeatherTest.Repository.Concretes
{
    public class CityRepository : BaseRepository, ICityRepository
    {
        public CityRepository(MvFactoryWeatherTestDbContext mvFactoryWeatherTestDbContext) : base(mvFactoryWeatherTestDbContext)
        {
        }

        public IEnumerable<City> FindAll()
        {
            var res = MvFactoryWeatherTestDbContext.Cities.ToList();
            return res;
        }
    }
}