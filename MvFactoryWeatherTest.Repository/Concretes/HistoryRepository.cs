﻿using System;
using System.Collections.Generic;
using System.Linq;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MVFactoryWeatherTest.Repository.Concretes
{
    public class HistoryRepository : BaseRepository, IHistoryRepository
    {
        public HistoryRepository(MvFactoryWeatherTestDbContext mvFactoryWeatherTestDbContext) : base(mvFactoryWeatherTestDbContext)
        {
        }

        public void Add(History wheaterHistory)
        {
            wheaterHistory.Date = DateTime.Now;
            MvFactoryWeatherTestDbContext.Add(wheaterHistory);
            Save();
        }

        public IEnumerable<History> FindByBranch(int branchId, DateTime desde, DateTime hasta)
        {
            var res = (
                from
                    h
                in
                    MvFactoryWeatherTestDbContext.History join b in MvFactoryWeatherTestDbContext.Branchs
                    on h.City.Id equals b.City.Id
                where 
                    b.Id == branchId &&
                    h.Date >= desde &&
                    h.Date <= hasta
                orderby
                    h.Date
                select
                    h
            ).ToList();

            return res;
        }
    }
}