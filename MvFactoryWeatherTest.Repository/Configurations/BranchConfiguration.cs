﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Configurations
{
    public class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {
        public void Configure(EntityTypeBuilder<Branch> builder)
        {
            builder.ToTable("Branchs");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Username).HasColumnType("VARCHAR(50)").IsRequired();
            builder.HasIndex(x => x.Username).IsUnique();

            builder.Property(x => x.Password).HasColumnType("VARCHAR(50)").IsRequired();

            builder.Property(x => x.Name).HasColumnType("VARCHAR(50)").IsRequired();
            builder.HasIndex(x => x.Name).IsUnique();

            builder.HasOne(x => x.City).WithMany(x => x.Branches).IsRequired();
        }
    }
}