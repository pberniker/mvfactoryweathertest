﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Configurations
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.ToTable("Cities");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).HasColumnType("VARCHAR(50)").IsRequired();
            builder.HasIndex(x => x.Name).IsUnique();

            builder.Property(x => x.State).HasColumnType("VARCHAR(50)");

            builder.Property(x => x.Country).HasColumnType("CHAR(2)").IsRequired();

            builder.Property(x => x.OpenWeatherId).HasColumnType("INTEGER").IsRequired();
            builder.HasIndex(x => x.OpenWeatherId).IsUnique();
        }
    }
}