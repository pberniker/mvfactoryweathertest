﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Configurations
{
    public class HistoryConfiguration : IEntityTypeConfiguration<History>
    {
        public void Configure(EntityTypeBuilder<History> builder)
        {
            builder.ToTable("History");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasKey(x => x.Id);

            builder.Property(x => x.JsonData).HasColumnType("VARCHAR(500)").IsRequired();

            builder.Property(x => x.Date).HasColumnType("DATETIME").IsRequired();

            builder.HasOne(x => x.City).WithMany(x => x.Histories).IsRequired().OnDelete(DeleteBehavior.Cascade);
        }
    }
}