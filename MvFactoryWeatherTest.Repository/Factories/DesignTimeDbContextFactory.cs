﻿using System.IO;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MVFactoryWeatherTest.Repository;

namespace MvFactoryWeatherTest.Repository.Factories
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MvFactoryWeatherTestDbContext>
    {
        public MvFactoryWeatherTestDbContext CreateDbContext(string[] args)
        {
            var path = $@"{Directory.GetCurrentDirectory()}\..\MvFactoryWeatherTest.Api";
            var configuration = new ConfigurationBuilder().SetBasePath(path).AddJsonFile("appsettings.json").Build();
            var connectionString = configuration.GetConnectionString("Default");
            var res = new MvFactoryWeatherTestDbContext(connectionString);
            return res;
        }
    }
}
