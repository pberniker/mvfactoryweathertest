﻿using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Interfaces
{
    public interface IBranchRepository
    {
        Branch Get(string username, string password);
    }
}