﻿using System.Collections.Generic;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> FindAll();
    }
}