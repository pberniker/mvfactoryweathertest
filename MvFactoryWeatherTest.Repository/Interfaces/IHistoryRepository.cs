﻿using System;
using System.Collections;
using System.Collections.Generic;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Interfaces
{
    public interface IHistoryRepository
    {
        void Add(History wheaterHistory);
        IEnumerable<History> FindByBranch(int branchId, DateTime from, DateTime to);
    }
}