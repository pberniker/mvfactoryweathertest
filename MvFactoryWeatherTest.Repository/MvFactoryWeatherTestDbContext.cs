﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Repository.Configurations;
using MVFactoryWeatherTest.Repository.Seeds;

namespace MVFactoryWeatherTest.Repository
{
    public class MvFactoryWeatherTestDbContext : DbContext
    {
        #region Members
        private readonly string _connectionString;
        #endregion

        #region Propertiese
        public virtual DbSet<Branch> Branchs { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<History> History { get; set; }
        #endregion

        #region Constructor
        public MvFactoryWeatherTestDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        #endregion

        #region Override

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
            optionsBuilder.UseLoggerFactory(new LoggerFactory(new[] { new DebugLoggerProvider() }));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SetConfiguratins(modelBuilder);
            SetSeeds(modelBuilder);
        }

        #endregion

        #region Private

        private static void SetConfiguratins(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CityConfiguration());
            modelBuilder.ApplyConfiguration(new BranchConfiguration());
            modelBuilder.ApplyConfiguration(new HistoryConfiguration());
        }

        private static void SetSeeds(ModelBuilder modelBuilder)
        {
            CitySeed.Execute(modelBuilder);
            BranchSeed.Execute(modelBuilder);
        }

        #endregion 
    }
}
