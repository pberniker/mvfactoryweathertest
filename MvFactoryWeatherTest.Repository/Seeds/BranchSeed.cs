﻿using Microsoft.EntityFrameworkCore;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Helpers;

namespace MVFactoryWeatherTest.Repository.Seeds
{
    public static class BranchSeed
    {
        public static void Execute(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Branch>().HasData(
                new
                    {
                        Id = 1,
                        Username = "User1",
                        Password = EncodeHelper.ToMd5("Pass1"),
                        Name = "Branch 1",    
                        CityId = 1
                    }
            );
        }
    }
}