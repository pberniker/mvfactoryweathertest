﻿using Microsoft.EntityFrameworkCore;
using MVFactoryWeatherTest.Domain;

namespace MVFactoryWeatherTest.Repository.Seeds
{
    public static class CitySeed
    {
        public static void Execute(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasData(
                new City { Id = 1, Name = "Ciudad Autónoma de Buenos Aires", State = "", Country = "AR", OpenWeatherId = 3433955 },
                new City { Id = 2, Name = "New York City", State = "NY", Country = "US", OpenWeatherId = 5128581 }
            );
        }
    }
}