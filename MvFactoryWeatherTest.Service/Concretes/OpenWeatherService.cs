﻿                            using System.Net;
using System.Net.Http;
using MvFactoryWeatherTest.Service.Interfaces;
using Newtonsoft.Json;

namespace MvFactoryWeatherTest.Service.Concretes
{
    public class OpenWeatherService : IOpenWeatherService
    {
        #region Members
        private readonly HttpClient _httpClient;
        private readonly string _url;
        private readonly string _key;
        #endregion

        #region Construct

        public OpenWeatherService(string url, string key)
        {
            _httpClient = new HttpClient();
            _url = url;
            _key = key;
        }

        #endregion

        #region Public

        public string GetData(int cityId)
        {
            var uri = $"{_url}/weather?id={cityId}&APPID={_key}";
            var responseMessage = _httpClient.GetAsync(uri).Result;

            var content = responseMessage.Content;
            var res = content.ReadAsStringAsync().Result;

            if (responseMessage.StatusCode == HttpStatusCode.InternalServerError) { /* throw new Exceptions.WebApiResponseException(str); */ }

            return res;
        }

        #endregion
    }
}