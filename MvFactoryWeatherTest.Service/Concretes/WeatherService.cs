﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvFactoryWeatherTest.Service.Interfaces;
using MVFactoryWeatherTest.Domain;
using MVFactoryWeatherTest.Repository.Interfaces;

namespace MvFactoryWeatherTest.Service.Concretes
{
    public class WeatherService : IWeatherService
    {
        #region Members
        private readonly IOpenWeatherService _openWeatherService;
        private readonly ICityRepository _cityRepository;
        private readonly IHistoryRepository _historyRepository;
        #endregion

        #region Construct

        public WeatherService(IOpenWeatherService openWeatherService, ICityRepository cityRepository, IHistoryRepository historyRepository)
        {
            _openWeatherService = openWeatherService;
            _cityRepository = cityRepository;
            _historyRepository = historyRepository;
        }

        #endregion

        #region Public

        public void Persist()
        {
            var cities = _cityRepository.FindAll();

            foreach (var city in cities)
            {
                PersistCurrent(city);
                // Todo: Realizar cuando se pague por el servicio.
                // PersistPrevious(city);
            }
        }

        public IEnumerable<History> GetHistoryByBranch(int branchId, DateTime from, DateTime to)
        {
            var res = _historyRepository.FindByBranch(branchId, from, to);
            return res;
        }

        #endregion

        #region Private

        private void PersistCurrent(City city)
        {
            var jsonData = _openWeatherService.GetData(city.OpenWeatherId);

            var history = new History
                {
                    City = city,
                    JsonData = jsonData
                };

            _historyRepository.Add(history);
        }

        private void PersistPrevious(City city)
        {
            var history = city.Histories.OrderBy(x => x.Date).FirstOrDefault();
        }

        #endregion
    }
}