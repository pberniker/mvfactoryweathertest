﻿namespace MvFactoryWeatherTest.Service.Interfaces
{
    public interface IOpenWeatherService
    {
        string GetData(int cityId);
    }
}