﻿using System;
using System.Collections.Generic;
using MVFactoryWeatherTest.Domain;

namespace MvFactoryWeatherTest.Service.Interfaces
{
    public interface IWeatherService
    {
        void Persist();
        IEnumerable<History> GetHistoryByBranch(int branchId, DateTime from, DateTime to);
    }
}