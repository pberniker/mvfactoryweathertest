### ¿Para qué sirve este repositorio? ###

* Se requiere una aplicación que permita obtener y persistir las condiciones climáticas en las sucursales de nuestro cliente usando la API abajo descripta. 
* El objetivo es que el cliente pueda generar reportes históricos del clima. 
* Modelar una solución de software que permita satisfacer estos requerimientos.

### Configuración ###

* Hacer clone del repositorio "https://pberniker@bitbucket.org/pberniker/mvfactoryweathertest.git".
* Copiar "\MvFactoryWeatherTest.Api\appsettings.example.json" en "\MVFactoryWeatherTest\src\MvFactoryWeatherTest.Api\appsettings.example.json".
* Abrir "\MvFactoryWeatherTest.Api\appsettings.example.json" y configurar "Default" "ConnectionStrings".
* Abrir la consola de comandos" ir a "\MvFactoryWeatherTest.Repository" y ejecutar: "dotnet ef database update"
* Realizar x POST a "http://localhost:60653/history" (Con Postman) simulando el cron.
* Ir a "http://localhost:50422/login" e ingresar con "User1", "Pass1".

### Tecnología ###

* Net Core 2.1.
* Web Api.
* Web MVC.
* EF Core 2.1
* Sql Server 2016.
* Visual Studio 2017.

### Aclaraciones ###

* Se entiende que las temperaturas climaticas cambian por ciudad y que tenemos mas de un cliente (tabla "Branchs") en la misma. Por eso la tabla "History" esta relacionada con "Cities" que tiene el id de ciudad de la api.
* Si esto no fuese asi y las temperasturas en una misma ciudad pueden ser diferentes, eliminariamos la tabla "Cities" y la tabla "History" pasaria a estar relacionada con "Branchs" que tendria las coordenada geograficas en vez del id.
* La actualizacion del clima en la tabla "History" se realiza ejecutando "POST http://localhost:60653/history".
* En "MVFactory.postman_collection.json" dejo la coleccion de Postman.

### Mejoras ###

* En la actualidad la app brinda información del clima que recolecto de la ejecución del cron. Pagar para poder usar el servicio de historial de la api y que le cron alimente la información del clima desde la fecha mas antigua que tenga, una semana para atras. Hacer que si el cliente pide un rango de fecha que aun no se tiene, la app sea capaz de obtenerlo por backraund (cron) y avisarle al mismo cuando esta.
* Realizar un reporte con graficos como "https://openweathermap.org/city" usando por ejemplo "https://developers.google.com/chart".
* Esperar 10 minutos (siguiendo la recomendaciones de la api: "https://openweathermap.org/appid#work" para realizar el proximo request en el caso de que la api no responda.
* Realziar las llamadas por multiples ids de ciudades ("Call for several city IDs").
* Hacer el front con Angular.
* La capa de api, servicio y repositorio que sea async.
* Dokerizar la app.
* Que la api sea https.
* Configuar los cors para permitir solo origenes desde el cliente "http://localhost:50422".
* Usar algo como "https://www.hangfire.io/" para actualziar el clima cada 1 hora.
* Restringir "POST" a "http://localhost:60653/history" por IP.

### Otros servicios ###

* Usar el servicio "Weather alerts" para generar alertas en caso de x condicion climatica.

